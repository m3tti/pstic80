module Player where

import Prelude
import Effect (Effect)
import Data.Array (range)
import TIC.Engine.Animation (animateSprite, Animation)
import TIC.Engine.Controls (Button(..))
import TIC.Engine.Collision (Solid, checkCellCollision)
import TIC.Engine.Common (Frame, EngineParams)

import TIC.API as TIC
import TIC.Engine.Sprite
  ( renderSprite
  , mkDefaultSprite
  , Position
  , SpriteId(..)
  , Colorkey(..)
  )

solids :: Array Solid
solids = map SpriteId $ range 20 27

data Action
  = Walk
  | Stay

type Player =
  { position :: Position
  , direction :: Int
  , currentSprite :: SpriteId
  , action :: Action
  }

squish :: SpriteId
squish = SpriteId 48

still :: SpriteId
still = SpriteId 49

initialState :: Player
initialState =
  { position: { x: 0, y: 0 }
  , direction: 1
  , currentSprite: still
  , action: Stay
  }

walk :: Animation
walk =
  { defaultSprite: still
  , sprites: [still, squish]
  , rate: 10
  }

stay :: Animation
stay =
  { defaultSprite: still
  , sprites: [still]
  , rate: 10
  }

update :: EngineParams -> Player -> Effect Player
update e s = collide e.tic s $ animate e.frame $ movePlayer e s

movePlayer :: EngineParams -> Player -> Player
movePlayer e s = case e.input of
  [Up] -> s{ position = s.position{ y = s.position.y-1 } , action = Walk }
  [Down] -> s{ position = s.position { y = s.position.y+1 }, action = Walk }
  [Left] -> s{ position = s.position { x = s.position.x-1 }, direction = 1, action = Walk }
  [Right] -> s{ position = s.position{ x = s.position.x+1 }, direction = 0, action = Walk }
  [Up, Right] -> s{ position = { y: s.position.y-1, x: s.position.x+1 }, action = Walk }
  [Down, Right] -> s{ position = { y: s.position.y+1, x: s.position.x+1 }, action = Walk }
  [Up, Left] -> s{ position = { y: s.position.y-1, x: s.position.x-1 }, action = Walk }
  [Down, Left] -> s{ position = { y: s.position.y+1, x: s.position.x-1 }, action = Walk }
  _ -> s{ action = Stay }

animate :: Frame -> Player -> Player
animate frame p =
  p{ currentSprite = actionAnimation p.action }
  where
    actionAnimation :: Action -> SpriteId
    actionAnimation Walk = animateSprite walk p.currentSprite frame
    actionAnimation Stay = animateSprite stay p.currentSprite frame

collide :: TIC.TIC -> Player -> Player -> Effect Player
collide t op np = do
  c <- checkCellCollision t solids np.position
  pure $ if c
    then np{ position = op.position }
    else np

render :: EngineParams -> Player -> Effect Unit
render e s =
  renderSprite e.tic $ mkDefaultSprite
          { id = s.currentSprite
          , position = s.position
          , flip = if s.direction == 0 then false else true
          , colorkey = Black
          }
