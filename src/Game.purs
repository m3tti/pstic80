module Game (gameLoop) where

import Prelude
import Effect (Effect)
import TIC.Engine (GameLoop)
import TIC.Engine.Common (EngineParams)
import TIC.API as TIC

import NPC.Horse as Horse
import Player as Player
import Map as Map
import Rules as Rules

type GameState =
  { player :: Player.Player
  , horse :: Horse.Horse
  }

initialState :: GameState
initialState =
  { horse: Horse.initialState
  , player: Player.initialState
  }

update :: EngineParams -> GameState -> Effect GameState
update e s = do
  player <- Player.update e s.player
  let horse = Horse.move e s.horse

  pure $ s{ player = Rules.collide s.player player horse
          , horse = horse
          }

render :: EngineParams -> GameState -> Effect Unit
render e s = do
  Map.render e
  Horse.render e s.horse
  Player.render e s.player
  pure unit

gameLoop :: TIC.TIC -> GameLoop GameState
gameLoop tic =
  { tic
  , initialState
  , update
  , render
  }
