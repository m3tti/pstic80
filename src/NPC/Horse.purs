module NPC.Horse where

import Prelude
import Effect (Effect)
import TIC.Engine.Animation (animateSprite, Animation)
import TIC.Engine.Sprite
  ( mkDefaultSprite
  , Colorkey(..)
  , renderSprite
  , Position
  , SpriteId(..)
  )
import TIC.Engine.Common (EngineParams, Frame, frameToInt)

type Horse =
  { position :: Position
  , direction :: Int
  , currentSprite :: SpriteId
  }

horseAnimation :: Animation
horseAnimation =
  { defaultSprite: SpriteId 7
  , sprites: [SpriteId 7, SpriteId 8]
  , rate: 10
  }

initialState :: Horse
initialState =
  { position: { x: 4, y: 5 }
  , direction: 1
  , currentSprite: SpriteId 7
  }

move :: EngineParams -> Horse -> Horse
move e h = animate h e.frame

render :: EngineParams -> Horse -> Effect Unit
render e h =
  renderSprite e.tic $ mkDefaultSprite
          { id = h.currentSprite
          , position = { x: 29
                       , y: 12
                       }
          , flip = true
          , colorkey = Black
          }

animate :: Horse -> Frame -> Horse
animate h frame =
  h{ currentSprite = animateSprite animateSpecial h.currentSprite frame }
  where
    animateSpecial = if (frameToInt frame) `mod` 60 == 0
                     then
                       horseAnimation{ defaultSprite = SpriteId 11
                                     , sprites = []
                                     , rate = 0
                                     }
                     else horseAnimation

