module Map where

import Prelude
import Effect (Effect)
import TIC.Engine.Sprite (Map, mkDefaultMap, renderMap)
import TIC.Engine.Common (EngineParams)
import TIC.API as TIC

render :: EngineParams -> Effect Unit
render e = renderMap e.tic $ mkDefaultMap
