module TIC.Engine.Animation where

import Prelude
import Data.Maybe (fromMaybe, Maybe(..))
import Data.Array (head, index, findIndex)
import TIC.Engine.Sprite (SpriteId)
import TIC.Engine.Common (Frame(..))

type Animation =
  { defaultSprite :: SpriteId
  , sprites :: Array SpriteId
  , rate :: Int
  }

animateSprite :: Animation -> SpriteId -> Frame -> SpriteId
animateSprite
  { rate, defaultSprite, sprites }
  currentSprite
  (Frame currentFrame) =
  if currentFrame `mod` rate == 0
    then nextSprite
    else currentSprite

  where
    nextSprite :: SpriteId
    nextSprite = fromMaybe defaultSprite
                 $ case findIndex (eq currentSprite) sprites of
                   Nothing -> head $ sprites
                   Just i -> index sprites (i+1)
