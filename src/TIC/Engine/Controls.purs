module TIC.Engine.Controls where

import Prelude
import Effect (Effect)
import Data.Array (catMaybes)
import Data.Maybe (Maybe(..))
import Data.Traversable (traverse)

import TIC.API as TIC

data Button
  = Up
  | Down
  | Left
  | Right
  | NA

getBtn :: TIC.TIC -> Effect (Array Button)
getBtn t = do
  buts <- traverse buttonActive [0,1,2,3]
  pure $ catMaybes buts
  where
    buttonActive :: Int -> Effect (Maybe Button)
    buttonActive i = do
      b <- TIC.btn t i
      pure $ if b
        then Just $ intToButton i
        else Nothing

intToButton :: Int -> Button
intToButton = case _ of
  0 -> Up
  1 -> Down
  2 -> Left
  3 -> Right
  _ -> NA

instance showButton :: Show Button where
  show = case _ of
    Up -> "UP"
    Down -> "DOWN"
    Left -> "Left"
    Right -> "Right"
    NA -> "NA"

btn :: TIC.TIC -> Button -> Effect Boolean
btn t b = case b of
  Up -> TIC.btn t 0
  Down -> TIC.btn t 1
  Left -> TIC.btn t 2
  Right -> TIC.btn t 3
  NA -> pure false

