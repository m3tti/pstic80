module TIC.Engine.Sprite where

import Prelude
import Effect (Effect)
import Data.Maybe (Maybe(..))
import TIC.API as TIC

type Position =
  { x :: Int
  , y :: Int
  }

newtype SpriteId = SpriteId Int
derive instance eqSprite :: Eq SpriteId

spriteToInt :: SpriteId -> Int
spriteToInt (SpriteId i) = i

type Size =
  { height :: Int
  , width :: Int
  }

data Colorkey
  = Transparent
  | Black
  | Purple
  | DarkBlue
  | DarkGray
  | DarkBrown
  | DarkGreen
  | Red
  | Gray
  | Blue
  | Brown
  | LightGray
  | LightGreen
  | Rosa
  | Cyan
  | Yellow
  | White

toColorkeyInt :: Colorkey -> Int
toColorkeyInt = case _ of
  Transparent -> -1
  Black -> 0
  Purple -> 1
  DarkBlue -> 2
  DarkGray -> 3
  DarkBrown -> 4
  DarkGreen -> 5
  Red -> 6
  Gray -> 7
  Blue -> 8
  Brown -> 9
  LightGray -> 10
  LightGreen -> 11
  Rosa -> 12
  Cyan -> 13
  Yellow -> 14
  White -> 15

newtype Scale = Scale Int

type Map =
  { startPoint :: Position
  , size :: Size -- size of map from start point how many
  , drawStartPoint :: Position
  , colorkey :: Colorkey
  , scale :: Scale
  }

fullscreenMapSize :: Size
fullscreenMapSize = { height: 17, width: 30 }

mkDefaultMap :: Map
mkDefaultMap =
  { startPoint: { x: 0, y: 0 }
  , size: fullscreenMapSize
  , drawStartPoint: { x: 0, y: 0 }
  , colorkey: Transparent
  , scale: Scale 1
  }

toTicMap :: Map -> TIC.Map
toTicMap m@{ scale: Scale s } =
  { x: m.startPoint.x
  , y: m.startPoint.y
  , h: m.size.height
  , w: m.size.width
  , sx: m.drawStartPoint.x
  , sy: m.drawStartPoint.y
  , colorkey: toColorkeyInt m.colorkey
  , scale: s
  , remap: Nothing
  }

renderMap :: TIC.TIC -> Map -> Effect Unit
renderMap t m = TIC.map t $ toTicMap m

type Sprite =
  { id :: SpriteId
  , position :: Position
  , colorkey :: Colorkey
  , scale :: Scale
  , flip :: Boolean
  , rotation :: Rotation
  , size :: Size
  }

mkDefaultSprite :: Sprite
mkDefaultSprite =
  { id: SpriteId 1
  , position: { x: 0, y: 0 }
  , colorkey: Black
  , scale: Scale 1
  , flip: false
  , rotation: Deg0
  , size: { height: 1, width: 1 }
  }

data Rotation
  = Deg0
  | Deg90
  | Deg180
  | Deg270

toDegree :: Rotation -> Int
toDegree = case _ of
  Deg0 -> 0
  Deg90 -> 90
  Deg180 -> 120
  Deg270 -> 270

toTicSprite :: Sprite -> TIC.Sprite
toTicSprite s@{ id: (SpriteId id), scale: (Scale scale) } =
  { id: id
  , x: s.position.x * 8
  , y: s.position.y * 8
  , colorkey: toColorkeyInt s.colorkey
  , flip: if s.flip then 0 else 1
  , scale: scale
  , rotate: toDegree s.rotation
  , width: s.size.width
  , height: s.size.height
  }

renderSprite :: TIC.TIC -> Sprite -> Effect Unit
renderSprite t = toTicSprite >>> TIC.spr t
