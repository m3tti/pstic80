module TIC.Engine.Collision where

import Prelude
import Effect (Effect)
import Data.Array (find)
import Data.Maybe (Maybe(..))
import Data.Traversable (traverse)
import TIC.Engine.Sprite (SpriteId(..), Position, Size)
import TIC.API as TIC

type Solid = SpriteId

checkCellCollision :: TIC.TIC -> Array Solid -> Position -> Effect Boolean
checkCellCollision t solids pos = do
  spriteId <- TIC.mget t pos.x pos.y
  case find (eq $ SpriteId spriteId) solids of
    Nothing -> pure false
    Just v -> pure true
