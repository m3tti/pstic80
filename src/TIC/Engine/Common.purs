module TIC.Engine.Common where

import Prelude

import TIC.API (TIC)
import TIC.Engine.Controls (Button)

newtype Time = Time Number
timeToNumber :: Time -> Number
timeToNumber (Time t) = t

newtype Frame = Frame Int
frameToInt :: Frame -> Int
frameToInt (Frame f) = f

type EngineParams =
  { tic :: TIC
  , time :: Time
  , frame :: Frame
  , input :: Array Button
  }

