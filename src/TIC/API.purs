module TIC.API where

import Prelude
import Control.Monad.Except (runExcept)
import Data.Either (Either(..))
import Foreign (Foreign, unsafeToForeign, readBoolean, readInt, readNumber)
import Effect (Effect)
import Data.Maybe (Maybe(..))

foreign import data TIC :: Type
foreign import tic :: TIC -> String -> Array Foreign -> Effect Foreign
foreign import setTicFunction :: forall a. TIC -> String -> Effect Unit -> Effect Unit

u = unsafeToForeign

print :: TIC -> Print -> Effect Unit
print f p = do
  _ <- tic f "print"
    [ unsafeToForeign p.text ]
  pure unit

cls :: TIC -> Effect Unit
cls t = do
  _ <- tic t "cls" []
  pure unit

spr :: TIC -> Sprite -> Effect Unit
spr t s = do
  _ <- tic t "spr"
    [ u s.id
    , u s.x
    , u s.y
    , u s.colorkey
    , u s.scale
    , u s.flip
    , u s.rotate
    , u s.width
    , u s.height
    ]
  pure unit

btn :: TIC -> Int -> Effect Boolean
btn t bid = do
  f <- tic t "btn" [ unsafeToForeign bid ]

  case runExcept $ readBoolean f of
    Left e ->
      pure false
    Right v ->
      pure v

map :: TIC -> Map -> Effect Unit
map t m = do
  _ <- tic t "map"
    [ ]
  pure unit

mget :: TIC -> Int -> Int -> Effect Int
mget t x y = do
  f <- tic t "mget"
    [ unsafeToForeign x
    , unsafeToForeign y
    ]
  case runExcept $ readInt f of
    Left e ->
      pure $ -1
    Right v ->
      pure v

time :: TIC -> Effect Number
time t = do
  f <- tic t "time" []
  case runExcept $ readNumber f of
    Left e ->
      pure $ -1.0
    Right v ->
      pure v

type Sprite =
  { id :: Int
  , x :: Int
  , y :: Int
  , colorkey :: Int
  , scale :: Int
  , flip :: Int
  , rotate :: Int
  , width :: Int
  , height :: Int
  }

type Print =
  { text :: String
  , x :: Int
  , y :: Int
  , color :: Int
  , fixed :: Boolean
  , scale :: Int
  , smallfont :: Boolean
  }

mkDefaultSprite :: Sprite
mkDefaultSprite =
  { id: 0
  , x: 0
  , y: 0
  , colorkey: -1
  , rotate: 0
  , flip: 0
  , scale: 1
  , width: 1
  , height: 1
  }

mkDefaultPrint :: Print
mkDefaultPrint =
  { text: ""
  , x: 0
  , y: 0
  , color: 15
  , fixed: false
  , scale: 1
  , smallfont: false
  }

type Map =
  { x :: Int
  , y :: Int
  , sx :: Int
  , sy :: Int
  , w :: Int
  , h :: Int
  , colorkey :: Int
  , scale :: Int
  , remap :: Maybe (Int -> Int -> Int -> Int)
  }

mkDefaultMap :: Map
mkDefaultMap =
  { x: 0
  , y: 0
  , sx: 0
  , sy: 0
  , w: 30
  , h: 17
  , colorkey: -1
  , scale: 1
  , remap: Nothing
  }

