module TIC.Engine
       ( GameLoop(..)
       , gameLoop
       )

       where

import Prelude
import Effect (Effect)
import Effect.Unsafe (unsafePerformEffect)
import Effect.Ref as EF

import TIC.API as TIC
import TIC.Engine.Controls (Button, getBtn)
import TIC.Engine.Common (Time(..), EngineParams, Frame(..))

type State a = EF.Ref a

mkState :: forall a. a -> State a
mkState state = unsafePerformEffect $ EF.new state

-- This type defines a game loop which is used by the Engine
-- The loop consists of
-- *update*: which basicly is used to modify the state of the game
-- The function gets the actual Button / Button Combination that
-- is currently clicked the Frame that is rendered and the gamestate
-- that was written last time
-- *render*: The function used to render all the sprites
-- *initialState*: The initial state of the game
type GameLoop a =
  { tic :: TIC.TIC
  , update :: EngineParams -> a -> Effect a
  , render :: EngineParams -> a -> Effect Unit
  , initialState :: a
  }

-- This function is used to start the tic game loop
-- It stores the state in a Ref and modifies it as it runs
gameLoop :: forall a. GameLoop a -> Effect Unit
gameLoop gl = TIC.setTicFunction gl.tic "TIC" do
  let st = mkState { tic: gl.tic, state: gl.initialState, frame: 0 }
  s <- EF.read st
  input <- getBtn gl.tic
  time <- TIC.time gl.tic
  let engine = { tic: s.tic, time: (Time time), input, frame: (Frame s.frame)}
  newState <- gl.update engine s.state
  _ <- gl.render engine newState
  EF.write { tic: gl.tic, state: newState, frame: s.frame + 1} st
