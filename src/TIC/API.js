exports.setTicFunction = function(tic) {
  return function (fnName) {
    return function(fn) {
      return function() {
        tic[fnName] = fn;
      }
    }
  }
}

exports.tic = function(tic) {
  return function(fnName) {
    return function(data) {
      return function() {
        return tic[fnName].apply(null, data);
      }
    }
  }
}
