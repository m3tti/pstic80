module Rules where

import Prelude
import NPC.Horse as Horse
import Player as Player

collide :: Player.Player -> Player.Player -> Horse.Horse -> Player.Player
collide op np h =
  if np.position == h.position
  then
    op
  else
    np
