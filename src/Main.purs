module Main where

import Prelude

import Effect (Effect)
import Game as Game
import TIC.Engine (gameLoop)
import TIC.API as TIC

main :: TIC.TIC -> Effect Unit
main tic = do
  gameLoop $ Game.gameLoop tic
