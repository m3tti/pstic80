BUILD_DIR=extern
install:
	npm install --save-dev browserify tinyify

build:
	spago bundle-module -t ${BUILD_DIR}/spago_bundle.js
	./node_modules/.bin/browserify -p tinyify ${BUILD_DIR}/tic.js > ${BUILD_DIR}/bundle.js
	cat ${BUILD_DIR}/header.js ${BUILD_DIR}/bundle.js > ${BUILD_DIR}/app.js
	rm -rf ${BUILD_DIR}/bundle.js ${BUILD_DIR}/spago_bundle.js ${BUILD_DIR}/shrinked_bundle.js
	du -h ${BUILD_DIR}/app.js

start:
	tic80 ${BUILD_DIR}/ghost.tic -code-watch ${BUILD_DIR}/app.js #-sprites ${BUILD_DIR}/pokedex.gif
